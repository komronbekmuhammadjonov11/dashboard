import React from 'react';
import ReactDOM from 'react-dom';
import App from './root/App';
import "antd/dist/antd.css";
import 'react-calendar/dist/Calendar.css';
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

