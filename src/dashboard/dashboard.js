import React , {useState} from 'react';
import {Col, Layout, Menu, Row} from 'antd';
import "../style/dashboard.scss";
import {Link, Route, Switch} from 'react-router-dom';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    DashboardOutlined,
    UserAddOutlined,
    UnorderedListOutlined,
    SettingOutlined
} from '@ant-design/icons';
import Settings from "./settings";
import TodoForm from "./todoForm";
import TodoList from "./todoList";

const Dashboard =(props)=> {

    const { Header, Sider, Content } = Layout;
    const [collapsed, setCollapsed]=useState(true);
        const toggle = () => {
            setCollapsed(!collapsed);
        };
    return (
        <div className="dashboard">
            <Layout>
                <Sider trigger={null} collapsible collapsed={collapsed}>
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1" icon={<DashboardOutlined />}>
                            <Link to="/">
                                Dashboard
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="2" icon={<UserAddOutlined />}>
                            <Link to="/form">
                                Developer
                            </Link>
                        </Menu.Item>
                        <Menu.Item key="4" icon={<SettingOutlined />}>
                            <Link to="/">
                                Settings
                            </Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 0 }}>
                        {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick: toggle,
                        })}
                        <div className="header-menu">
                            <div className="search-input">
                                <input type="text" placeholder="Search..."/>
                            </div>
                            <div className="bell-icon">
                                <i className="far fa-bell"></i>
                            </div>
                            <div className="user-img">
                                <img src="/images/my.jpg" alt="Komronbek"/>
                            </div>
                            <div className="setting-icon">
                                <i className="fas fa-cog"></i>
                            </div>
                        </div>
                    </Header>
                    <Content
                        className="site-layout-background"
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            minHeight: 280,
                        }}
                    >
                      <Switch>
                          <Route path="/" exact component={Settings}/>
                          <Route path="/form" exact component={TodoForm}/>
                          <Route path="/list" exact component={TodoList}/>
                      </Switch>
                    </Content>
                    <div className="setting-footer">
                        <Row>
                            <Col span={24}>
                                <div className="text">
                                    © 2021 My Dashboard with
                                    <i className="fas fa-heart"></i>
                                    by Themesbrand.
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Layout>
            </Layout>
        </div>
    );
};

export default Dashboard;