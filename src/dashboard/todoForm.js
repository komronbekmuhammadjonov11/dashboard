import React, {useContext, useState} from 'react';
import {Button, Col, Input, Row, Select} from 'antd';
import "../style/todoForm.scss";
import { UserOutlined } from '@ant-design/icons';
import {DeveloperContext} from "../root/App";
const { Option } = Select;
function TodoForm(props) {
    const {data, setData}=useContext(DeveloperContext);
    const [name, setName]=useState("");
    const [value, setValue]=useState("1");
    function addDeveloper() {
        if (name){
            let newObject={
                id:Date.now(),
                name,
                role_id:value
            };
            let newArray=data.concat(newObject);
            setData(newArray);
            localStorage.setItem("developer-list", JSON.stringify(newArray));
            setName("");
        }
    }
    console.log(data);
    function deleteDeveloper(id) {
        let newArray=[];
        newArray=data.filter(item=>{
            return item.id!==id;
        });
        setData(newArray);
        localStorage.setItem("developer-list", JSON.stringify(newArray));
    }
    return (
        <div className="todo-form">
            <Row>
                <Col span={10} offset={7}>
                   <div className="input-group">
                       <Input
                           size="large"
                           placeholder="Add developer..."
                           prefix={<UserOutlined />}
                           value={name}
                           onChange={(e)=>setName(e.target.value)}
                       />
                       <Select size={"large"} value={value}  style={{ width: 120 }} onChange={(e)=>setValue(e)}>
                           <Option value="1">Front End</Option>
                           <Option value="2">Back End</Option>
                           <Option value="3">Full Stack</Option>
                           <Option value="4">Design</Option>
                       </Select>
                       <Button type="primary" size="large" onClick={addDeveloper}>
                           Add
                       </Button>
                   </div>
                    <div className="tab-list">
                        <table>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            {data.map((item, index)=>{
                                const {role_id}=item;
                                return <tr key={item.id}>
                                    <td>{++index}</td>
                                    <td>{item.name}</td>
                                    <td>{role_id==="1"? "Front End": role_id==="2"? "Back End": role_id==="3"? "Full Stack": role_id==="4"? "Design":""}</td>
                                    <td>
                                        <Button type="danger" onClick={()=>deleteDeveloper(item.id)}>
                                            Delete
                                        </Button>
                                    </td>
                                </tr>
                            })}
                            </tbody>
                        </table>
                    </div>
                </Col>
            </Row>
        </div>
    );
}

export default TodoForm;