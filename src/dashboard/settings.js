import React, {useState, useContext, useEffect} from 'react';
import "../style/settings.scss"
import {LineChart, Line, YAxis, XAxis, CartesianGrid, Tooltip, Legend, PieChart, Pie, Cell} from 'recharts';
import {Button, Col, Row} from "antd";
import {Link} from "react-router-dom";
import {Calendar} from "react-calendar";
import {DeveloperContext} from "../root/App";
function Settings(props) {
    const [value, onChange] = useState(new Date());
    const {data, setData}=useContext(DeveloperContext);
    const [frontCount, setFrontCount]=useState(0);
    const [backCount, setBackCount]=useState(0);
    const [fullCount, setFullCount]=useState(0);
    const [designCount, setDesignCount]=useState(0);
    // console.log(value.getDate());
    let InitialPieData = [
        {
            "name": "FrontEnd",
            "value":0
        },
        {
            "name": "BackEnd",
            "value": 0
        },
        {
            "name": "FullStack",
            "value": 0
        },
        {
            "name": "Design",
            "value": 0
        }
    ];
    function getMontFunction(count) {
        switch (count) {
            case 0 :
                return "January";
            break;
            case 1:
                return "February";
            break;
            case 2:
                return "March";
                break;
            case 3:
                return "April";
                break;
            case 4:
                return "May";
                break;
            case 5:
                return "June";
                break;
            case 6:
                return "July";
                break;
            case 7:
                return "August";
                break;
            case 8:
                return "September";
                break;
            case 9:
                return "October";
                break;
            case 10:
                return "November";
                break;
            case 11:
                return "December";
                break;
            default: return ""
        }
    }
    const [pieDate, setPieDate]=useState(InitialPieData);
    useEffect(()=>{
        let count1=0;
        let count2=0;
        let count3=0;
        let count4=0;
        data.forEach((item)=>{
            if (item.role_id==="1"){
                count1++;
            }else if (item.role_id==="2"){
                count2++;
            }else  if (item.role_id==="3") {
                count3++;
            }else if (item.role_id==="4") {
                count4++;
            }
        });
        let newPieData = [
            {
                "name": "FrontEnd",
                "value": Math.floor(count1/data.length*100)
            },
            {
                "name": "BackEnd",
                "value": Math.floor(count2/data.length*100)
            },
            {
                "name": "FullStack",
                "value": Math.floor(count3/data.length*100)
            },
            {
                "name": "Design",
                "value": Math.floor(count4/data.length*100)
            }
        ];
        setFrontCount(count1);
        setBackCount(count2);
        setFullCount(count3);
        setDesignCount(count4);
        setPieDate(newPieData);
    }, [data]);
    const chartData = [
        {
            "name": `${getMontFunction(value.getMonth())} ${value.getDate()-2}`,
            "FrontEnd": 10,
            "BackEnd": 11,
            "FullStack":23,
            "Design":14
        },
        {
            "name": `${getMontFunction(value.getMonth())} ${value.getDate()-1}`,
            "FrontEnd": 7,
            "BackEnd": 18,
            "FullStack":22,
            "Design":17
        },
        {
            "name": `${getMontFunction(value.getMonth())} ${value.getDate()}`,
            "FrontEnd": 20,
            "BackEnd": 14,
            "FullStack":27,
            "Design":7
        },
        {
            "name": `${getMontFunction(value.getMonth())} ${value.getDate()+1}`,
            "FrontEnd": 17,
            "BackEnd": 15,
            "FullStack":29,
            "Design":10
        },
        {
            "name": `${getMontFunction(value.getMonth())} ${value.getDate()+2}`,
            "FrontEnd": 16,
            "BackEnd": 11,
            "FullStack":18,
            "Design":19
        }
    ];



    let COLORS = ['#0095FF', '#FF0000', '#FF8042', '#00C49F'];



    const CustomTooltip = ({ active, payload, label }) => {
        if (active) {
            return (
                <div className="custom-tooltip" style={{ backgroundColor: '#ffff', padding: '5px', border: '1px solid #cccc' }}>
                    <label>{`${payload[0].name} : ${payload[0].value}%`}</label>
                </div>
            );
        }

        return null;
    };

    function deleteDeveloper(id) {
        let newArray=[];
        newArray=data.filter(item=>{
            return item.id!==id;
        });
        setData(newArray);
        localStorage.setItem("developer-list", JSON.stringify(newArray));
    }

    return (
        <div className="settings">
            <div className="settings-header">
                <div className="header-left">
                    <div className="title">Dashboard</div>
                    <p>Welcome to My Dashboard</p>
                </div>
                <div className="header-right">
                    <button className="btn">
                        <span className="btn-images-content">
                            <span className="btn-images"></span>
                        </span>
                        Settings
                    </button>
                </div>
            </div>
            <div className="card-group">
                <Row>
                    <Col span={6}>
                        <div className="card">
                            <div className="card-body">
                                <div className="img-content">
                                    <img src="/images/ui.png" alt="Front End"/>
                                </div>
                                <div className="text-content">
                                    <div className="name">Front End</div>
                                    <div className="count">{frontCount}</div>
                                </div>
                            </div>
                            <div className="card-footer">
                                <Link  className="link">
                                    Batafsil
                                    <div className="right-arrow-img"></div>
                                </Link>
                            </div>
                        </div>
                    </Col>
                    <Col span={6}>
                        <div className="card">
                            <div className="card-body">
                                <div className="img-content">
                                    <img src="/images/backend.png" alt="Back End"/>
                                </div>
                                <div className="text-content">
                                    <div className="name">Back End</div>
                                    <div className="count">{backCount}</div>
                                </div>
                            </div>
                            <div className="card-footer">
                                <Link  className="link">
                                    Batafsil
                                    <div className="right-arrow-img"></div>
                                </Link>
                            </div>
                        </div>
                    </Col>
                    <Col span={6}>
                        <div className="card">
                            <div className="card-body">
                                <div className="img-content">
                                    <img src="/images/full.png" alt="FullStack"/>
                                </div>
                                <div className="text-content">
                                    <div className="name"> FullStack</div>
                                    <div className="count">{fullCount}</div>
                                </div>
                            </div>
                            <div className="card-footer">
                                <Link  className="link">
                                    Batafsil
                                    <div className="right-arrow-img"></div>
                                </Link>
                            </div>
                        </div>
                    </Col>
                    <Col span={6}>
                        <div className="card">
                            <div className="card-body">
                                <div className="img-content">
                                    <img src="/images/design.png" alt="Design"/>
                                </div>
                                <div className="text-content">
                                    <div className="name">Design</div>
                                    <div className="count">{designCount}</div>
                                </div>
                            </div>
                            <div className="card-footer">
                                <Link  className="link">
                                    Batafsil
                                    <div className="right-arrow-img"></div>
                                </Link>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
            <div className="graphics">
                <Row>
                    <Col span={15}>
                        <div className="graphics-content">
                            <LineChart width={730} height={250} data={chartData}
                                       margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis dataKey="name" />
                                <YAxis />
                                <Tooltip />
                                <Legend />
                                <Line type="monotone" dataKey="FrontEnd" stroke="#0095FF" />
                                <Line type="monotone" dataKey="BackEnd" stroke="#FF0000" />
                                <Line type="monotone" dataKey="FullStack" stroke="#FF8042" />
                                <Line type="monotone" dataKey="Design" stroke="#00C49F" />
                            </LineChart>
                        </div>
                    </Col>
                    <Col span={9}>
                        <div className="progress-bar">
                            <PieChart width={350} height={300}>
                                <Pie data={pieDate} color="#000000" dataKey="value" nameKey="name" cx="50%" cy="50%" outerRadius={120} fill="#8884d8" >
                                    {
                                        pieDate.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                                    }
                                </Pie>
                                <Tooltip content={CustomTooltip(true, pieDate)} />
                                <Legend />
                            </PieChart>
                        </div>
                    </Col>
                </Row>
            </div>
            <div className="settings-bottom">
                <Row>
                    <Col span={8}>
                        <div className="react-calendar-content">
                            <Calendar
                                onChange={onChange}
                                value={value}
                            />
                        </div>
                    </Col>
                    <Col span={16}>
                        <div className="tab-list">
                            <table>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {data.map((item, index)=>{
                                    const {role_id}=item;
                                    return <tr key={item.id}>
                                        <td>{++index}</td>
                                        <td>{item.name}</td>
                                        <td>{role_id==="1"? "Front End": role_id==="2"? "Back End": role_id==="3"? "Full Stack": role_id==="4"? "Design":""}</td>
                                        <td>
                                            <Button type="danger" onClick={()=>deleteDeveloper(item.id)}>
                                                Delete
                                            </Button>
                                        </td>
                                    </tr>
                                })}
                                </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default Settings;