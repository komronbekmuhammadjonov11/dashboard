import React, {createContext, useState} from 'react';
import './App.css';
import Dashboard from "../dashboard/dashboard";
import {BrowserRouter , Switch, Route} from "react-router-dom";
export const DeveloperContext = createContext(null);
function App() {
    const [data, setData] = useState(localStorage.getItem("developer-list") ? JSON.parse(localStorage.getItem("developer-list")) : []);
    return (
        <div className="App">
            <div className="App-content">
                <DeveloperContext.Provider value={{data, setData}}>
                    <BrowserRouter>
                        <Switch>
                            <Route path="/" component={Dashboard}/>
                        </Switch>
                    </BrowserRouter>
                </DeveloperContext.Provider>
            </div>
        </div>
  );
}

export default App;
